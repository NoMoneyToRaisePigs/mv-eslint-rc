# x-eslint-rc
This repository contains eslint rule set for exchange repositories 

## Installation
`$ yarn add -D git+https://github.com/monacohq/x-eslint-rc.git `

## Usage
In your `eslintrc` file, extend your desired ruleset as below: 
```
...
extends: [
    // Choose one of below    
    'exchange',             // for none typescript repo
    'exchange/typescript'   // for typescript repo
],
...
```