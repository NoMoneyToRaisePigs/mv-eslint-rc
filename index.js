const vueTs = require('./packages/vue-ts');
const ts = require('./packages/ts');

module.exports = {
  vueTs,
  ts,
}