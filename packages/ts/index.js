module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    jest: true,
  },
  parser: '@typescript-eslint/parser',

  plugins: [
    '@typescript-eslint',
    'import',
  ],
  extends: [
    'standard',
    'plugin:@typescript-eslint/recommended',
    'plugin:import/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
  ],
  ignorePatterns: ['**/e2e/*', '.eslintrc.js', '**/node_modules/**/*'],
  rules: {
    'import/order': [
      'error',
      {
        'newlines-between': 'always',
        groups: [
          'builtin',
          'external',
          'internal',
          'parent',
          'sibling',
          'index',
        ],
        pathGroups: [
          {
            pattern: 'src/**',
            group: 'internal',
          },
        ],
      },
    ],
    'import/no-unresolved': 'off',
    'comma-dangle': ['error', 'always-multiline'],
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    semi: [2, 'never'],
    'spaced-comment': 'error',
    camelcase: 'off',
    'no-useless-constructor': 'off',
    'import/no-absolute-path': 'off',
    'object-curly-spacing': ['error', 'always'],
    'standard/no-callback-literal': 'off',
    '@typescript-eslint/no-this-alias': 'off',
    '@typescript-eslint/no-useless-constructor': 'off',
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/no-empty-function': 'warn',
    '@typescript-eslint/semi': ['error', 'never'],
    '@typescript-eslint/member-delimiter-style': [
      'error',
      {
        multiline: {
          delimiter: 'none',
          requireLast: false,
        },
        singleline: {
          delimiter: 'semi',
          requireLast: false,
        },
      },
    ],
    // Some generic rules are not working with TS
    // disable them and use the extended rule from @typescript-eslint/eslint-plugin
    '@typescript-eslint/object-curly-spacing': ['error', 'always'],
    'array-bracket-newline': [
      'error',
      {
        multiline: true,
      },
    ],
    '@typescript-eslint/space-before-blocks': ['error'],
    '@typescript-eslint/func-call-spacing': ['error'],
    'padding-line-between-statements': 'error',
    'lines-between-class-members': 'error',
    'lines-around-directive': 'error',
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': [
      'warn',
      {
        argsIgnorePattern: '^_',
        varsIgnorePattern: '^_',
        caughtErrorsIgnorePattern: '^_',
      },
    ],
    'linebreak-style': 'error',
    'newline-after-var': 'error',
    'newline-per-chained-call': 'error',
    'newline-before-return': 'error',
    '@typescript-eslint/no-extra-semi': 'off',
    '@typescript-eslint/no-var-requires': 'off',
  },
  settings: {
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts', '.tsx'],
    },
    'import/internal-regex': 'src/',
    'import/resolver': {
      typescript: {
        alwaysTryTypes: true,
        project: ['tsconfig.json'],
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
      node: {
        alwaysTryTypes: true,
        projects: ['tsconfig.json'],
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
    },
  },
}
