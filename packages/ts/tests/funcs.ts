export function TestParamRequiredErrorExample (param) {
  return {
    param,
  }
}

export function TestParamRequiredCorrectExample (param: string) {
  return {
    param,
  }
}

// should auto remove Delimiter when lint --fix
export interface TestDelimiterSingleLine {
  name: string
  age: string
}

export interface TestDelimiterMultipleLine {
  name: string
  age: number
}

export const ObjectCurlyNewLineExample = {
  bar: 'This is a bar.',
  baz: {
    qux: 'This is a qux',
  },
  difficult: 'to read',
}

const { bar, baz, difficult } = ObjectCurlyNewLineExample

const arrayNewLineExample = [1, 2, 3, 4, 5, 6, 7, 8, 6, 6, 6, 6, 6, 6, 6, 6, 6, 77777.888888, 99999, 555555]
