module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    jest: true,
    'vue/setup-compiler-macros': true,
  },
  globals: {
    globalThis: 'readonly',
    Sentry: true,
    defineProps: 'readonly',
    defineEmits: 'readonly',
    defineExpose: 'readonly',
    withDefaults: 'readonly',
  },
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: {
      // Script parser for `<script>`
      js: 'espree',

      // Script parser for `<script lang='ts'>`
      ts: '@typescript-eslint/parser',

      // Script parser for vue directives (e.g. `v-if=` or `:attribute=`)
      // and vue interpolations (e.g. `{{variable}}`).
      // If not specified, the parser determined by `<script lang ='...'>` is used.
      '<template>': 'espree',
    },
    sourceType: 'module',
  },
  plugins: [
    'vue',
    '@typescript-eslint',
    'import',
  ],
  extends: [
    '@vue/typescript/recommended',
    'plugin:vue/vue3-recommended',
    '@vue/standard',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
  ],
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)',
      ],
      env: {
        jest: true,
      },
    },
  ],
  ignorePatterns: ['**/e2e/*', '.eslintrc.js', 'typescript.js', '**/node_modules/**/*'],
  rules: {
    'import/order': [
      'error',
      {
        'newlines-between': 'always',
        groups: [
          'builtin',
          'external',
          'internal',
          'parent',
          'sibling',
          'index',
        ],
        pathGroups: [
          {
            pattern: 'src/**',
            group: 'internal',
          },
        ],
      },
    ],
    'import/no-unresolved': 'off',
    'comma-dangle': ['error', 'always-multiline'],
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    semi: [2, 'never'],
    'spaced-comment': 'error',
    camelcase: 'off',
    'vue/max-attributes-per-line': [
      'error',
      {
        singleline: {
          max: 3,
        },
        multiline: {
          max: 1,
        },
      },
    ],
    'vue/no-v-html': 'off',
    'vue/require-default-prop': 'off',
    'vue/require-v-for-key': 'off',
    'vue/no-unused-components': 'off',
    'vue/no-unused-vars': 'error',
    'vue/name-property-casing': 'off',
    'vue/singleline-html-element-content-newline': 'off',
    'vue/multiline-html-element-content-newline': 'error',
    'vue/html-self-closing': 'off',
    'no-useless-constructor': 'off',
    'import/no-absolute-path': 'off',
    'vue/component-definition-name-casing': ['error', 'kebab-case'],
    'vue/script-setup-uses-vars': 'error',
    'object-curly-spacing': ['error', 'always'],
    'standard/no-callback-literal': 'off',
    'vue/multi-word-component-names': 'off',
    '@typescript-eslint/no-this-alias': 'off',
    '@typescript-eslint/no-useless-constructor': 'off',
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/no-empty-function': 'warn',
    '@typescript-eslint/semi': ['error', 'never'],
    '@typescript-eslint/member-delimiter-style': [
      'error',
      {
        multiline: {
          delimiter: 'none',
          requireLast: false,
        },
        singleline: {
          delimiter: 'semi',
          requireLast: false,
        },
      },
    ],
    // Some generic rules are not working with TS
    // disable them and use the extended rule from @typescript-eslint/eslint-plugin
    '@typescript-eslint/object-curly-spacing': ['error', 'always'],
    'array-bracket-newline': [
      'error',
      {
        multiline: true,
      },
    ],
    '@typescript-eslint/space-before-blocks': ['error'],
    '@typescript-eslint/func-call-spacing': ['error'],
    'padding-line-between-statements': 'error',
    'lines-between-class-members': 'error',
    'lines-around-directive': 'error',
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': [
      'warn',
      {
        argsIgnorePattern: '^_',
        varsIgnorePattern: '^_',
        caughtErrorsIgnorePattern: '^_',
      },
    ],
    'linebreak-style': 'error',
    'newline-after-var': 'error',
    'newline-per-chained-call': 'error',
    'newline-before-return': 'error',
    '@typescript-eslint/no-extra-semi': 'off',
    '@typescript-eslint/no-var-requires': 'off',
  },
  settings: {
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts', '.tsx'],
    },
    'import/internal-regex': 'src/',
    'import/resolver': {
      typescript: {
        alwaysTryTypes: true,
        project: ['tsconfig.json'],
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
      node: {
        alwaysTryTypes: true,
        projects: ['tsconfig.json'],
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
    },
  },
}
